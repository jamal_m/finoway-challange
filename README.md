
# Finoway Challange Project

Build Restful CRUD API using Spring Boot, PostgreSql, JPA, Hibernate, SpringSecurity and...

## Steps to Setup

**1. Clone the application**

```bash
git clone https://gitlab.com/jamal_m/finoway-challange
```

**2. Create PostgreSql database**
```bash
create database ngo
```

**3. Change Postgres username and password as per your installation**

+ open `src/main/resources/application.properties`
+ change `spring.datasource.username` and `spring.datasource.password` as per your Postgres installation

**4. Run the app using maven**

```bash
mvn spring-boot:run
```
The app will start running at <http://localhost:8080>

## Explore Rest APIs

The app defines following CRUD APIs.

### Auth

| Method | Url | Decription | Sample Valid Request Body | 
| ------ | --- | ---------- | --------------------------- |
| POST   | /api/auth/signup | Sign up | [JSON](#signup) |
| POST   | /api/auth/signin | Log in | [JSON](#signin) |
| POST   | /api/auth/refreshtoken | Refresh token | [JSON](#refreshToken) |

### Users

| Method | Url | Description | Sample Valid Request Body |
| ------ | --- | ----------- | ------------------------- |
| GET    | /api/users | all user |  [JSON](#getAllUser)|
| PUT    | /api/users/{id} | Update userX (If profile belongs to logged in userX) | [JSON](#update)|
| PUT    | /api/users/{id}/updateEmail | Update userX email | [JSON](#updateEmail)|
| DELETE | /api/users/{id} | Delete userX (For logged in userX |[JSON](#delete) |


Test them using postman or any other rest client.

## Sample Valid JSON Request Bodys

##### <a id="signup">Sign Up -> /api/auth/signup</a>
```json
{
  "firstName": "jamal",
  "lastName": "mousavi",
  "username": "3300209331",
  "birthDate": "1995-03-06",
  "timeZone": 1,
  "phone": "09360929519",
  "email": "j.mousavi1373@gmail.com",
  "password": "1234"
}
```

##### <a id="signin">Log In -> /api/auth/signin</a>
```json
{
  "usernameOrEmail": "3300209331",
  "password": "1234",
  "rememberMe": true
}
```
##### <a id="refreshToken">Refresh Token -> /api/auth/refreshtoken</a>
```json
{
  "refreshToken": "{{refresh_token}}"
}
```

##### <a id="getAllUser">Get All User -> /api/users</a>
```json

```
##### <a id="update">Update User -> /api/users/id</a>
```json
{
  "firstName": "sgwa",
  "lastName": "mousavi",
  "birthDate": "2022-11-24",
  "timeZone": 2,
  "phone": "09135925011",
  "email": "j.mousavi1373@gmail.com"
}
```
##### <a id="updateEmail">Update User Email Only -> /api/users/updateEmail</a>
```json
{
  "email": "j.mouasvi1988@gmail.com"
}
```

##### <a id="delete">Delete User -> /api/users/id</a>
```json

```
