package ir.finoway.challenge.utils;

import ir.finoway.challenge.model.user.User_x;
import javax.annotation.processing.Generated;
import org.springframework.stereotype.Component;

@Generated(
    value = "org.mapstruct.ap.MappingProcessor",
    date = "2022-11-24T22:28:25-0800",
    comments = "version: 1.4.2.Final, compiler: javac, environment: Java 18.0.1.1 (Oracle Corporation)"
)
@Component
public class UserMappingImpl implements UserMapping {

    @Override
    public User_x updateWithNullAsNoChange(User_x source, User_x target) {
        if ( source == null ) {
            return null;
        }

        if ( source.getCreatedAt() != null ) {
            target.setCreatedAt( source.getCreatedAt() );
        }
        if ( source.getUpdatedAt() != null ) {
            target.setUpdatedAt( source.getUpdatedAt() );
        }
        if ( source.getId() != null ) {
            target.setId( source.getId() );
        }
        if ( source.getFirstName() != null ) {
            target.setFirstName( source.getFirstName() );
        }
        if ( source.getLastName() != null ) {
            target.setLastName( source.getLastName() );
        }
        if ( source.getBirthDate() != null ) {
            target.setBirthDate( source.getBirthDate() );
        }
        if ( source.getTimeZone() != null ) {
            target.setTimeZone( source.getTimeZone() );
        }
        if ( source.getUsername() != null ) {
            target.setUsername( source.getUsername() );
        }
        if ( source.getPhone() != null ) {
            target.setPhone( source.getPhone() );
        }
        if ( source.getPassword() != null ) {
            target.setPassword( source.getPassword() );
        }
        if ( source.getEmail() != null ) {
            target.setEmail( source.getEmail() );
        }
        if ( source.getRefreshToken() != null ) {
            target.setRefreshToken( source.getRefreshToken() );
        }

        return target;
    }
}
