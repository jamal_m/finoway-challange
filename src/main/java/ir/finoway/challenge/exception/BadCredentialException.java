package ir.finoway.challenge.exception;

import ir.finoway.challenge.payload.ApiResponse;
import org.springframework.http.HttpStatus;
import org.springframework.web.bind.annotation.ResponseStatus;

@ResponseStatus(HttpStatus.UNAUTHORIZED)
public class BadCredentialException extends RuntimeException {
	private static final long serialVersionUID = 1L;

	private ApiResponse apiResponse;

	public BadCredentialException(ApiResponse apiResponse, Throwable cause) {
		super(cause);
		this.apiResponse = apiResponse;
	}

	public BadCredentialException(String message) {
		super(message);
	}

	public BadCredentialException(String message, Throwable cause) {
		super(message, cause);
	}

	public ApiResponse getApiResponse() {
		return apiResponse;
	}
}
