package ir.finoway.challenge.controller;

import ir.finoway.challenge.exception.NgoaException;
import ir.finoway.challenge.model.auth.RefreshToken;
import ir.finoway.challenge.model.user.User_x;
import ir.finoway.challenge.payload.ApiResponse;
import ir.finoway.challenge.payload.JwtAuthenticationResponse;
import ir.finoway.challenge.payload.auth.TokenRefreshRequest;
import ir.finoway.challenge.payload.auth.TokenRefreshResponse;
import ir.finoway.challenge.repository.UserRepository;
import ir.finoway.challenge.utils.NumberUtil;
import ir.finoway.challenge.exception.TokenRefreshException;
import ir.finoway.challenge.payload.auth.LoginRequest;
import ir.finoway.challenge.security.JwtTokenProvider;
import ir.finoway.challenge.service.interfaces.auth.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.authentication.AuthenticationManager;
import org.springframework.security.authentication.UsernamePasswordAuthenticationToken;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.servlet.support.ServletUriComponentsBuilder;

import javax.validation.Valid;
import java.net.URI;


@RestController
@RequestMapping("/api/auth")
public class AuthController {

    private AuthenticationManager authenticationManager;

    private UserRepository userRepository;

    private PasswordEncoder passwordEncoder;

    private JwtTokenProvider jwtTokenProvider;

    private RefreshTokenService refreshTokenService;

    @Autowired
    public AuthController(AuthenticationManager authenticationManager, UserRepository userRepository, PasswordEncoder passwordEncoder, JwtTokenProvider jwtTokenProvider, RefreshTokenService refreshTokenService) {
        this.authenticationManager = authenticationManager;
        this.userRepository = userRepository;
        this.passwordEncoder = passwordEncoder;
        this.jwtTokenProvider = jwtTokenProvider;
        this.refreshTokenService = refreshTokenService;
    }

    @PostMapping("/signin")
    public ResponseEntity<JwtAuthenticationResponse> authenticateUser(@Valid @RequestBody LoginRequest loginRequest) {
        Authentication authentication = authenticationManager.authenticate(
                new UsernamePasswordAuthenticationToken(loginRequest.getUsernameOrEmail(), loginRequest.getPassword()));
        SecurityContextHolder.getContext().setAuthentication(authentication);

        String jwt = jwtTokenProvider.generateToken(authentication);
        User_x userX = userRepository.findByUsernameOrEmail(loginRequest.getUsernameOrEmail(), loginRequest.getUsernameOrEmail())
                .orElseThrow(() -> new UsernameNotFoundException(String.format("User not found with this username or email: %s", loginRequest.getUsernameOrEmail())));
        RefreshToken refreshToken = null;
        if (loginRequest.getRememberMe()) {
            refreshToken = refreshTokenService.createRefreshToken(userX.getId(), 2);
        } else {
            refreshToken = new RefreshToken();
            refreshToken.setToken("");
        }
        return ResponseEntity.ok(new JwtAuthenticationResponse(jwt, refreshToken.getToken(), userX));
    }


    @PostMapping("/signup")
    public ResponseEntity<ApiResponse> RegisterUser(@Valid @RequestBody User_x signUpRequest) {
        if (Boolean.TRUE.equals(userRepository.existsByUsername(signUpRequest.getUsername()))) {
            throw new NgoaException(HttpStatus.BAD_REQUEST, "Username is already taken");
        }
        if (!NumberUtil.isNumber(signUpRequest.getUsername())) {
            throw new NgoaException(HttpStatus.BAD_REQUEST, "Username is not valid(only number)");
        }
        if (signUpRequest.getUsername().length() != 10) {
            throw new NgoaException(HttpStatus.BAD_REQUEST, "Username should be 10 char");
        }
        if (Boolean.TRUE.equals(userRepository.existsByEmail(signUpRequest.getEmail()))) {
            throw new NgoaException(HttpStatus.BAD_REQUEST, "Email is already taken");
        }
        if (Boolean.TRUE.equals(userRepository.existsByPhone(signUpRequest.getPhone()))) {
            throw new NgoaException(HttpStatus.BAD_REQUEST, "phone number is already taken");
        }
        //User_x userX = userService.addUser(signUpRequest);
        String password = passwordEncoder.encode(signUpRequest.getPassword());
        signUpRequest.setPassword(password);
        User_x result = userRepository.save(signUpRequest);
        URI location = ServletUriComponentsBuilder.fromCurrentContextPath().path("/api/users/{userId}")
                .buildAndExpand(result.getId()).toUri();
        return ResponseEntity.created(location).body(new ApiResponse(Boolean.TRUE, "User registered successfully"));
    }

    @PostMapping("/refreshtoken")
    public ResponseEntity<?> refreshToken(@Valid @RequestBody TokenRefreshRequest request) {
        String requestRefreshToken = request.getRefreshToken();

        return refreshTokenService.findByToken(requestRefreshToken)
                .map(refreshTokenService::verifyExpiration)
                .map(RefreshToken::getUser_x)
                .map(user -> {
                    String token = jwtTokenProvider.generateTokenFromUsername(user.getId());
                    return ResponseEntity.ok(new TokenRefreshResponse(token, requestRefreshToken));
                })
                .orElseThrow(() -> new TokenRefreshException(requestRefreshToken,
                        "Refresh token is not in database!"));
    }

}
