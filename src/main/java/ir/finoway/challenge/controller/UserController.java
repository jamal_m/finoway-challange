package ir.finoway.challenge.controller;

import ir.finoway.challenge.model.user.User_x;
import ir.finoway.challenge.payload.ApiResponse;
import ir.finoway.challenge.payload.EmailUpdateRequest;
import ir.finoway.challenge.payload.PagedResponse;
import ir.finoway.challenge.security.CurrentUser;
import ir.finoway.challenge.security.UserPrincipal;
import ir.finoway.challenge.service.interfaces.UserService;
import ir.finoway.challenge.utils.AppConstants;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import javax.validation.Valid;

@RestController
@RequestMapping("/api/users")
public class UserController {

	private UserService userService;

	public UserController(UserService userService) {
		this.userService = userService;
	}

	@PutMapping("/{id}")
	public ResponseEntity<User_x> updateUser(@Valid @RequestBody User_x newUserX,
											 @PathVariable(name = "id") Long id, @CurrentUser UserPrincipal currentUser) {
		User_x updatedUSer = userService.updateUser(id,newUserX, currentUser);
		return new ResponseEntity< >(updatedUSer, HttpStatus.OK);
	}
	@PutMapping("/updateEmail")
	public ResponseEntity<ApiResponse> updateUserEmail(@Valid @RequestBody EmailUpdateRequest emailUpdateRequest,
											 @CurrentUser UserPrincipal currentUser) {
		ApiResponse response  = userService.updateUserEmail(emailUpdateRequest, currentUser);
		HttpStatus status = response.getSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
		return new ResponseEntity<>(response, status);
	}
	@DeleteMapping("/{id}")
	public ResponseEntity<ApiResponse> deleteS(@CurrentUser UserPrincipal currentUser, @PathVariable Long id) {
		ApiResponse response = userService.deleteUser(id,currentUser);
		HttpStatus status = response.getSuccess() ? HttpStatus.OK : HttpStatus.BAD_REQUEST;
		return new ResponseEntity<>(response, status);
	}
	@GetMapping()
	public ResponseEntity<PagedResponse<User_x>> getAll(
			@RequestParam(name = "page", required = false, defaultValue = AppConstants.DEFAULT_PAGE_NUMBER) Integer page,
			@RequestParam(name = "size", required = false, defaultValue = AppConstants.DEFAULT_PAGE_SIZE) Integer size,
			@RequestParam(name = "sort_field", required = false,defaultValue = AppConstants.DEFAULT_SORT_FIELD_SUPPORTER) String sortProperty,
			@RequestParam(name = "sort_dir", required = false,defaultValue = AppConstants.DEFAULT_SORT_DIRECTION) String sortDirection) {
		PagedResponse<User_x> all = userService.getAll(page, size, sortDirection, sortProperty);
		return new ResponseEntity<>(all, HttpStatus.OK);
	}

}
