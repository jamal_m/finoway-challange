package ir.finoway.challenge;

import ir.finoway.challenge.security.JwtAuthenticationFilter;
import org.modelmapper.ModelMapper;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.boot.autoconfigure.domain.EntityScan;
import org.springframework.context.annotation.Bean;
import org.springframework.data.convert.Jsr310Converters;

import javax.annotation.PostConstruct;
import java.util.TimeZone;

@SpringBootApplication
@EntityScan(basePackageClasses = { FinowayChallangeApplication.class, Jsr310Converters.class })

public class FinowayChallangeApplication {

	private static final String ZONE_ID_ISTANBUL = "Europe/Istanbul";

	public static void main(String[] args) {

			TimeZone.setDefault(TimeZone.getTimeZone(ZONE_ID_ISTANBUL));
		SpringApplication.run(FinowayChallangeApplication.class, args);

	}

	@PostConstruct
	void init() {
		TimeZone.setDefault(TimeZone.getTimeZone("UTC"));
	}

	@Bean
	public JwtAuthenticationFilter jwtAuthenticationFilter() {
		return new JwtAuthenticationFilter();
	}

	@Bean
	public ModelMapper modelMapper() {
		return new ModelMapper();
	}

}
