package ir.finoway.challenge.service.interfaces.auth;


import ir.finoway.challenge.model.auth.RefreshToken;

import java.util.Optional;


public interface RefreshTokenService {
	RefreshToken createRefreshToken(Long userId, int type);
	Optional<RefreshToken> findByToken(String token);
	RefreshToken verifyExpiration(RefreshToken token);
	int deleteByUserId(Long userId);
}