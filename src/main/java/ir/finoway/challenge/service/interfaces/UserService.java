package ir.finoway.challenge.service.interfaces;

import ir.finoway.challenge.model.user.User_x;
import ir.finoway.challenge.payload.ApiResponse;
import ir.finoway.challenge.payload.EmailUpdateRequest;
import ir.finoway.challenge.payload.PagedResponse;
import ir.finoway.challenge.security.UserPrincipal;
import org.springframework.security.core.userdetails.UserDetails;

public interface UserService {

    User_x updateUser(Long supporterId, User_x newUser, UserPrincipal currentUser);

    ApiResponse deleteUser(Long id, UserPrincipal currentUser);

    PagedResponse<User_x> getAll(int page, int size, String sortDirection, String sortProperty);

    ApiResponse updateUserEmail(EmailUpdateRequest emailUpdateRequest, UserPrincipal currentUser);

    UserDetails loadUserById(Long id);
}