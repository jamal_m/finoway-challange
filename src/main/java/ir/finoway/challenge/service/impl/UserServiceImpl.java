package ir.finoway.challenge.service.impl;

import ir.finoway.challenge.exception.ResourceNotFoundException;
import ir.finoway.challenge.model.user.User_x;
import ir.finoway.challenge.payload.ApiResponse;
import ir.finoway.challenge.payload.EmailUpdateRequest;
import ir.finoway.challenge.payload.PagedResponse;
import ir.finoway.challenge.repository.UserRepository;
import ir.finoway.challenge.security.UserPrincipal;
import ir.finoway.challenge.service.interfaces.UserService;
import ir.finoway.challenge.utils.AppUtils;
import ir.finoway.challenge.utils.UserMapping;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.security.crypto.password.PasswordEncoder;
import org.springframework.stereotype.Service;

import javax.transaction.Transactional;

@Service
public class UserServiceImpl implements UserService {

	private UserRepository userRepository;

	private PasswordEncoder passwordEncoder;

	UserMapping userMapping;

	@Autowired
	public UserServiceImpl(UserRepository userRepository, PasswordEncoder passwordEncoder, UserMapping userMapping) {
		this.userRepository = userRepository;
		this.passwordEncoder = passwordEncoder;
		this.userMapping = userMapping;
	}


	@Override
	public User_x updateUser(Long supporterId, User_x newUser, UserPrincipal currentUser) {
		User_x user = userRepository.findById(supporterId).orElseThrow(()->new ResourceNotFoundException("user","id",supporterId));
		userMapping.updateWithNullAsNoChange(newUser, user);
		return userRepository.save(user);
	}
	@Override
	public ApiResponse updateUserEmail(EmailUpdateRequest emailUpdateRequest, UserPrincipal currentUser) {
		int result=userRepository.updateUserEmail(emailUpdateRequest.getEmail(),currentUser.getId());
		if (result==1) {
			return new ApiResponse(Boolean.TRUE, "You successfully update userEmail  with id: " + currentUser.getId());
		}else{
			return new ApiResponse(Boolean.FALSE, "can't update userEmail  with id: " + currentUser.getId());
		}

	}
	@Override
	public ApiResponse deleteUser(Long id, UserPrincipal currentUser) {
		User_x userX = userRepository.findById(id)
				.orElseThrow(() -> new ResourceNotFoundException("User", "id", id));
		userRepository.deleteById(userX.getId());
		return new ApiResponse(Boolean.TRUE, "You successfully deleted user with id: " + id);
	}
	@Override
	public PagedResponse<User_x> getAll(int page, int size, String sortDirection, String sortProperty) {
		Sort.Direction direction = AppUtils.convertStrToDirection(sortDirection);
		AppUtils.validatePageNumberAndSize(page, size);
		Pageable pageable = PageRequest.of(page, size, direction, sortProperty);
		Page<User_x> stuffs = userRepository.findAll(pageable);
		return new PagedResponse<>(stuffs.getContent(), stuffs.getNumber(), stuffs.getSize(),
				stuffs.getTotalElements(), stuffs.getTotalPages(), stuffs.isLast());
	}
	@Override
	@Transactional
	public UserDetails loadUserById(Long id) {
		User_x userX = userRepository.findById(id).orElseThrow(() -> new UsernameNotFoundException(String.format("User not found with id: %s", id)));

		return UserPrincipal.create(userX);
	}
}
