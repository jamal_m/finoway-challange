package ir.finoway.challenge.service.impl.auth;

import ir.finoway.challenge.model.auth.RefreshToken;
import ir.finoway.challenge.repository.UserRepository;
import ir.finoway.challenge.repository.auth.RefreshTokenRepository;
import ir.finoway.challenge.exception.TokenRefreshException;
import ir.finoway.challenge.service.interfaces.auth.RefreshTokenService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.Instant;
import java.util.Optional;
import java.util.UUID;


@Service
public class RefreshTokenServiceImpl implements RefreshTokenService {

    private Long refreshTokenDurationMs=1000000l;
    private Long refreshTokenDurationMsMobile=5000000000l;

    @Autowired
    private RefreshTokenRepository refreshTokenRepository;

    @Autowired
    private UserRepository userRepository;

    public Optional<RefreshToken> findByToken(String token) {
        return refreshTokenRepository.findByToken(token);
    }

    public RefreshToken createRefreshToken(Long userId,int type) {
        RefreshToken refreshToken = new RefreshToken();

        refreshToken.setUser_x(userRepository.findById(userId).get());
        if (type==1) {
            refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMsMobile));
        }else {
            refreshToken.setExpiryDate(Instant.now().plusMillis(refreshTokenDurationMs));
        }
        refreshToken.setToken(UUID.randomUUID().toString());

        refreshToken = refreshTokenRepository.save(refreshToken);
        return refreshToken;
    }

    public RefreshToken verifyExpiration(RefreshToken token) {
        if (token.getExpiryDate().compareTo(Instant.now()) < 0) {
            refreshTokenRepository.delete(token);
            throw new TokenRefreshException(token.getToken(), "Refresh token was expired. Please make a new signin request");
        }
        return token;
    }

    @Transactional
    public int deleteByUserId(Long userId) {
        return 1;
    }
}

