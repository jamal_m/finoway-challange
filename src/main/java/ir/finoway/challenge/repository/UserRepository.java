package ir.finoway.challenge.repository;

import ir.finoway.challenge.model.user.User_x;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;
import org.springframework.stereotype.Repository;
import org.springframework.transaction.annotation.Transactional;

import javax.validation.constraints.NotBlank;
import java.util.Optional;

@Repository
public interface UserRepository extends JpaRepository<User_x, Long> {
	Optional<User_x> findByUsername(@NotBlank String username);
	Optional<User_x> findByEmail(@NotBlank String email);
	Optional<User_x> findById(@NotBlank Long id);
	Boolean existsByUsername(@NotBlank String username);
	Boolean existsByPhone(@NotBlank String phone);
	Boolean existsByEmail(@NotBlank String email);
	Optional<User_x> findByUsernameOrEmail(String username, String email);

	@Transactional
	@Modifying
	@Query("update User_x user_x set user_x.email = :email where user_x.id = :id")
	int updateUserEmail(@Param("email") String salt, @Param("id") Long id);
}
