package ir.finoway.challenge.utils;

public class AppConstants {

	public static final int MIN_PASSWORD_LENGTH = 4;

	public static final int MAX_PASSWORD_LENGTH = 30;

	public static final String DEFAULT_PAGE_NUMBER = "0";

	public static final String DEFAULT_PAGE_SIZE = "30";

	public static final String DEFAULT_SORT_DIRECTION ="ASC" ;

	public static final String DEFAULT_SORT_FIELD_SUPPORTER ="createdAt" ;

	public static final int MAX_PAGE_SIZE = 30;
}
