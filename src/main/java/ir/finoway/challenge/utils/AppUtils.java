package ir.finoway.challenge.utils;

import ir.finoway.challenge.payload.ApiResponse;
import ir.finoway.challenge.exception.BadRequestException;
import ir.finoway.challenge.exception.NgoaException;
import org.springframework.data.domain.Sort;
import org.springframework.http.HttpStatus;

public class AppUtils {
	public static void validatePageNumberAndSize(int page, int size) {
		if (page < 0) {
			throw new NgoaException(HttpStatus.BAD_REQUEST, "Page number cannot be less than zero.");
		}

		if (size < 0) {
			throw new NgoaException(HttpStatus.BAD_REQUEST, "Size number cannot be less than zero.");
		}

		if (size > AppConstants.MAX_PAGE_SIZE) {
			throw new NgoaException(HttpStatus.BAD_REQUEST, "Page size must not be greater than " + AppConstants.MAX_PAGE_SIZE);
		}
	}
	public static Sort.Direction convertStrToDirection(String sortDir){
		Sort.Direction direction;
		if (sortDir.equalsIgnoreCase("asc")) {
			direction = Sort.Direction.ASC;
		} else if (sortDir.equalsIgnoreCase("desc")) {
			direction = Sort.Direction.DESC;
		} else {
			direction = Sort.Direction.ASC;
		}
		return direction;
	}
	public static void validatePassword(String password) {
		if (password==null){
			throw new BadRequestException(new ApiResponse(false, "password is null", HttpStatus.BAD_REQUEST));
		}
		if (password.length() < AppConstants.MIN_PASSWORD_LENGTH) {
			throw new BadRequestException(new ApiResponse(false, "password length cannot be less than "+AppConstants.MIN_PASSWORD_LENGTH,HttpStatus.BAD_REQUEST));
		}
		if (password.length() >AppConstants.MAX_PASSWORD_LENGTH) {
			throw new BadRequestException(new ApiResponse(false, "password length must not be greater than "+AppConstants.MIN_PASSWORD_LENGTH,HttpStatus.BAD_REQUEST));
		}
	}
}
