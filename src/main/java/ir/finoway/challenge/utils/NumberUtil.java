package ir.finoway.challenge.utils;

import java.util.concurrent.ThreadLocalRandom;

public class NumberUtil {
    public static boolean isNumber(String number){
        if (number.matches("[0-9]+")) {
            return true;
        }
        return false;
    }
    public static int generateRandomNumber(int digitCount) {
        final int maxNumber= (int) Math.pow(10,digitCount);
        final int minNumber=(int)Math.pow(10,digitCount-1);
        // Generate random integers
        int intRandom = ThreadLocalRandom.current().nextInt(minNumber, maxNumber);

        return intRandom;
    }
}
