package ir.finoway.challenge.model.user.enums;

import com.fasterxml.jackson.annotation.JsonCreator;
import com.fasterxml.jackson.annotation.JsonFormat;
import com.fasterxml.jackson.annotation.JsonValue;
@JsonFormat(shape = JsonFormat.Shape.OBJECT)
public enum TimeZone {
    TIME_ZONE_ONE(1),
    TIME_ZONE_TWO(2),
    TIME_ZONE_THREE(3),
    TIME_ZONE_FOUR(4);

    @JsonValue
    private int genderCode;

    private TimeZone(int depCode) {
        this.genderCode = depCode;
    }

    public int getGenderCode() {
        return genderCode;
    }

    @JsonCreator
    public static TimeZone getTimeZoneFromCode(int value) {
        for (TimeZone timeZone : TimeZone.values()) {
            if (timeZone.getGenderCode()==value) {
                return timeZone;
            }
        }
        return null;
    }


}