package ir.finoway.challenge.model.user;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonProperty;
import ir.finoway.challenge.model.auth.RefreshToken;
import ir.finoway.challenge.utils.AppConstants;
import ir.finoway.challenge.model.audit.DateAudit;
import ir.finoway.challenge.model.user.enums.TimeZone;
import lombok.*;
import org.hibernate.annotations.DynamicUpdate;

import javax.persistence.*;
import javax.validation.constraints.Email;
import javax.validation.constraints.NotBlank;
import javax.validation.constraints.Size;
import java.time.LocalDate;


@EqualsAndHashCode(callSuper = true)
@Data
@Entity
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Table(name = "users", uniqueConstraints = { @UniqueConstraint(columnNames = { "username" }) })
@Inheritance(strategy = InheritanceType.JOINED)
@DynamicUpdate
public class User_x extends DateAudit {
	private static final long serialVersionUID = 1L;

	@Id
	@GeneratedValue(strategy = GenerationType.SEQUENCE )
	@Column(name = "id")
	private Long id;

	@NotBlank
	@Column(name = "first_name")
	@Size(max = 40)
	private String firstName;

	@NotBlank
	@Column(name = "last_name")
	@Size(max = 40)
	private String lastName;

	@Column(name = "birth_date")
	private LocalDate birthDate;

	@Column(name = "time_zone")
	private TimeZone timeZone;

	@NotBlank
	@Column(name = "username")
	private String username;

	@NotBlank
	@Column(name = "phone")
	@Size(max = 11, min = 11)
	private String phone;

	@NotBlank
	@JsonProperty(access = JsonProperty.Access.WRITE_ONLY)
	@Size(max = AppConstants.MAX_PASSWORD_LENGTH,min = AppConstants.MIN_PASSWORD_LENGTH)
	@Column(name = "password")
	private String password;

	@Size(max = 60)
	@Column(name = "email")
	@Email
	private String email;

	@JsonIgnore
	@OneToOne(mappedBy = "user_x")
	private RefreshToken refreshToken;

}
