package ir.finoway.challenge.payload;

import ir.finoway.challenge.model.user.User_x;
import lombok.Data;

@Data
public class JwtAuthenticationResponse {
	private String accessToken;
	private String tokenType = "Bearer";
	private String refreshToken;
	private User_x user_x;

	public JwtAuthenticationResponse(String accessToken, String refreshToken, User_x user_x) {
		this.accessToken = accessToken;
		this.refreshToken = refreshToken;
		this.user_x = user_x;
	}
}
