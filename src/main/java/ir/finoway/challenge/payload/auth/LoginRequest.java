package ir.finoway.challenge.payload.auth;


import lombok.Data;

import javax.validation.constraints.NotBlank;

@Data
public class LoginRequest {
    @NotBlank
    private String usernameOrEmail;

    @NotBlank
    private String password;


    private Boolean rememberMe;




}
